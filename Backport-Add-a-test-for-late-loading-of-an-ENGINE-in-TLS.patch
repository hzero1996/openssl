From 9ffd4a34681feb9968719905b366276e7425e2a2 Mon Sep 17 00:00:00 2001
From: Matt Caswell <matt@openssl.org>
Date: Wed, 29 Nov 2023 11:30:07 +0000
Subject: [PATCH] Add a test for late loading of an ENGINE in TLS

Confirm that using an ENGINE works as expected with TLS even if it is
loaded late (after construction of the SSL_CTX).

(cherry picked from commit a9c97da4910648790387d035afb12963158778fb)

Reviewed-by: Tomas Mraz <tomas@openssl.org>
Reviewed-by: Todd Short <todd.short@me.com>
(Merged from https://github.com/openssl/openssl/pull/22865)

(cherry picked from commit dda9208cef52670e6c832cbadaa3e08ad535ac30)
---
 test/sslapitest.c | 56 +++++++++++++++++++++++++++++++++++------------
 1 file changed, 42 insertions(+), 14 deletions(-)

diff --git a/test/sslapitest.c b/test/sslapitest.c
index 2191b297d0..e0274f12f7 100644
--- a/test/sslapitest.c
+++ b/test/sslapitest.c
@@ -10128,6 +10128,27 @@ end:
 }
 
 #if !defined(OPENSSL_NO_TLS1_2) && !defined(OPENSSL_NO_DYNAMIC_ENGINE)
+
+static ENGINE *load_dasync(void)
+{
+    ENGINE *e;
+
+    if (!TEST_ptr(e = ENGINE_by_id("dasync")))
+        return NULL;
+
+    if (!TEST_true(ENGINE_init(e))) {
+        ENGINE_free(e);
+        return NULL;
+    }
+
+    if (!TEST_true(ENGINE_register_ciphers(e))) {
+        ENGINE_free(e);
+        return NULL;
+    }
+
+    return e;
+}
+
 /*
  * Test TLSv1.2 with a pipeline capable cipher. TLSv1.3 and DTLS do not
  * support this yet. The only pipeline capable cipher that we have is in the
@@ -10143,6 +10164,8 @@ end:
  * Test 4: Client has pipelining enabled, server does not: more data than all
  *         the available pipelines can take
  * Test 5: Client has pipelining enabled, server does not: Maximum size pipeline
+ * Test 6: Repeat of test 0, but the engine is loaded late (after the SSL_CTX
+ *         is created)
  */
 static int test_pipelining(int idx)
 {
@@ -10155,25 +10178,28 @@ static int test_pipelining(int idx)
     size_t written, readbytes, offset, msglen, fragsize = 10, numpipes = 5;
     size_t expectedreads;
     unsigned char *buf = NULL;
-    ENGINE *e;
-
-    if (!TEST_ptr(e = ENGINE_by_id("dasync")))
-        return 0;
+    ENGINE *e = NULL;
 
-    if (!TEST_true(ENGINE_init(e))) {
-        ENGINE_free(e);
-        return 0;
+    if (idx != 6) {
+        e = load_dasync();
+        if (e == NULL)
+            return 0;
     }
 
-    if (!TEST_true(ENGINE_register_ciphers(e)))
-        goto end;
-
     if (!TEST_true(create_ssl_ctx_pair(libctx, TLS_server_method(),
                                        TLS_client_method(), 0,
                                        TLS1_2_VERSION, &sctx, &cctx, cert,
                                        privkey)))
         goto end;
 
+    if (idx == 6) {
+        e = load_dasync();
+        if (e == NULL)
+            goto end;
+        /* Now act like test 0 */
+        idx = 0;
+    }
+
     if (!TEST_true(create_ssl_objects(sctx, cctx, &serverssl,
                                       &clientssl, NULL, NULL)))
         goto end;
@@ -10309,9 +10335,11 @@ end:
     SSL_free(clientssl);
     SSL_CTX_free(sctx);
     SSL_CTX_free(cctx);
-    ENGINE_unregister_ciphers(e);
-    ENGINE_finish(e);
-    ENGINE_free(e);
+    if (e != NULL) {
+        ENGINE_unregister_ciphers(e);
+        ENGINE_finish(e);
+        ENGINE_free(e);
+    }
     OPENSSL_free(buf);
     if (fragsize == SSL3_RT_MAX_PLAIN_LENGTH)
         OPENSSL_free(msg);
@@ -10684,7 +10712,7 @@ int setup_tests(void)
     ADD_ALL_TESTS(test_serverinfo_custom, 4);
 #endif
 #if !defined(OPENSSL_NO_TLS1_2) && !defined(OPENSSL_NO_DYNAMIC_ENGINE)
-    ADD_ALL_TESTS(test_pipelining, 6);
+    ADD_ALL_TESTS(test_pipelining, 7);
 #endif
     ADD_ALL_TESTS(test_handshake_retry, 16);
     ADD_ALL_TESTS(test_multi_resume, 5);
-- 
2.33.0

